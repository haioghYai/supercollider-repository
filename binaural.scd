{SinOsc.ar([[300,MouseX.kr(250,350)],660],0,0.1)}.scope // dos notas en diferentes canales, con esto puedes hacer binaurales



{SinOsc.ar([[300,MouseX.kr(250,350)],[300,MouseX.kr(250,350)]],0,0.1)}.scope


(
{
var freq, baseline, maxDelta, lateralShift, source;       //local variables to hold objects as we build the patch up
	maxDelta=400;
	baseline=MouseX.kr(0,500);
	freq=[[(baseline+MouseY.kr(-1* maxDelta,maxDelta)),baseline],[(baseline+MouseY.kr(-1* maxDelta,maxDelta)),baseline]];
	source=SinOsc.ar(freq,0,0.1);

source // last thing is returned from function in curly brackets, i.e. this is the final sound we hear
}.scope;
)