
// ejemplificando cómo definir un sintetizador para utilizarlo despues

SynthDef("sine", { | freq = 220, amp = 1 |
	Out.ar(0, SinOsc.ar( freq ) * amp )
}).send(s);

z = Synth("sine")

z.set(\freq,120)
z.set(\amp,1)
z.run(false)
z.run(true)
z.free

