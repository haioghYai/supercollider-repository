
// guitoder
// ukiyo-neko systems
//
// es un 'vocoder' pero quiero que tome la frecuencia fundamental del input y aplique un BPF al resultado,
// quiero que se mueva con un lag, pa que suene mas chido
(
SynthDef(\modulo,{arg  modfreq=1, moddepth=0.01, modfreqfreq=1, modfreqdepth;   //make sure there are control arguments to affect!
	Out.ar(0,
    SoundIn.ar()*4 * (moddepth*SinOsc.ar(modfreq+(modfreqdepth*SinOsc.ar(modfreqfreq))))!2)
}).add;
)

s.scope;

(

var w, modfreqslider, moddepthslider, modfreqfreqslider, modfreqdepthslider, synth;

w=Window("frequency modulation", Rect(100, 400, 400, 300));
w.view.decorator = FlowLayout(w.view.bounds);

a = Synth(\modulo);


modfreqslider= EZSlider(w, 300@50, "modfreq", ControlSpec(1, 100, 'linear', 0.1, 1), {|ez|  a.set(\modfreq, ez.value)});
w.view.decorator.nextLine;


modfreqfreqslider= EZSlider(w, 300@50, "modfreqfreq", ControlSpec(0, 100, 'linear', 1, 440), {|ez|
a.set(\modfreqfreq, ez.value)});
w.view.decorator.nextLine;

modfreqdepthslider= EZSlider(w, 300@50, "modfreqdepth", ControlSpec(0, 500, 'linear', 1, 440), {|ez|
a.set(\modfreqdepth, ez.value)});
w.view.decorator.nextLine;

moddepthslider= EZSlider(w, 300@50, "moddepth", ControlSpec(0.01, 10, 'linear', 0.01, 0.01), {|ez|  a.set(\moddepth, ez.value)});

w.front;
)
