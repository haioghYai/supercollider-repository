 {// below we declare our variables with 'var'
	var input, fBLoopIn, fBLoopOut, processing, speed;
	speed =LFNoise0.kr(0.5,2,2.1);
	// input is our sound source, a little noise -- note the lowered amplitude
	// of 0.15
	input =SoundIn.ar(0,4);
	// fBLoopIn is our feedback loop insertion point.
	fBLoopIn =LocalIn.ar(1);
	// in processing, we mix the input with the feedback loop
	// the delay time of the DelayN UGen is controlled by the
	// 'speed' variable. The gain is now fixed at 1.1.
	processing = input +DelayN.ar(fBLoopIn,3.5, MouseY.kr(0,5),MouseX.kr(0,1.5));
	// use a resonant low-pass filter that moves at various rates
	// determined by the 'speed' variable with frequencies between 400 - 1200
	//processing =RLPF.ar(processing,LFNoise0.kr(speed,400,800),0.15);
	// fBLoopOut is our feedback loop output point
	fBLoopOut =LocalOut.ar(processing);
	// signal threshold a "low-rent gate"
	//processing = processing.thresh(0.45);
	// our limiter
	//processing =Limiter.ar(processing);
	processing =RLPF.ar(processing,300);
	// add some equal-power panning
	processing =Pan2.ar(processing, 0);
	// notice removed brackets because Pan2 is a multi-channel UGen
	// and SC has multichannel expansion for free ;)
	Out.ar(0, processing); }.play


(
Ndef(\verb, {
	var input, output, delrd, sig, deltimes;

	// Choose which sort of input you want by (un)commenting these lines:
	//input = Pan2.ar(PlayBuf.ar(1, b, loop: 0), -0.5); // buffer playback, panned halfway left
	input = SoundIn.ar([0,1]); // TAKE CARE of feedback - use headphones
	//input = Dust2.ar([0.1, 0.01]); // Occasional clicks

	// Read our 4-channel delayed signals back from the feedback loop
	delrd = LocalIn.ar(4);

	// This will be our eventual output, which will also be recirculated
	output = input + delrd[[0,1]];

	// Cross-fertilise the four delay lines with each other:
	sig = [output[0]+output[1], output[0]-output[1], delrd[2]+delrd[3], delrd[2]-delrd[3]];
	sig = [sig[0]+sig[2], sig[1]+sig[3], sig[0]-sig[2], sig[1]-sig[3]];
	// Attenutate the delayed signals so they decay:
	sig = sig * [0.4, 0.37, 0.333, 0.3] - MouseY.kr(-0.3, 1);

	// Here we give delay times in milliseconds, convert to seconds,
	// then compensate with ControlDur for the one-block delay
	// which is always introduced when using the LocalIn/Out fdbk loop
	deltimes = [101, 143, 165, 177] * 0.001 - ControlDur.ir+MouseX.kr(-3,10);

	// Apply the delays and send the signals into the feedback loop
	LocalOut.ar(DelayC.ar(sig, deltimes, deltimes));

	// Now let's hear it:
	output

}).play
)


(
Ndef(\verb, {
	var input, output, delrd, sig, deltimes;

	// Choose which sort of input you want by (un)commenting these lines:
	//input = Pan2.ar(PlayBuf.ar(1, b, loop: 0), -0.5); // buffer playback, panned halfway left
	input = SoundIn.ar([0,1]); // TAKE CARE of feedback - use headphones
	//input = Dust2.ar([0.1, 0.01]); // Occasional clicks

	// Read our 4-channel delayed signals back from the feedback loop
	delrd = LocalIn.ar(4);

	// This will be our eventual output, which will also be recirculated
	output = input + delrd[[0,1]];

	sig = output ++ delrd[[2,3]];
	// Cross-fertilise the four delay lines with each other:
	sig = ([ [1, 1, 1, 1],
	 [1, -1, 1, -1],
	 [1, 1, -1, -1],
	 [1, -1, -1, 1]] * sig).sum;
	// Attenutate the delayed signals so they decay:
	sig = sig * [0.4, 0.37, 0.333, 0.3];

	// Here we give delay times in milliseconds, convert to seconds,
	// then compensate with ControlDur for the one-block delay
	// which is always introduced when using the LocalIn/Out fdbk loop
	deltimes = [101, 143, 165, 177] * 0.001 - ControlDur.ir;

	// Apply the delays and send the signals into the feedback loop
	LocalOut.ar(DelayC.ar(sig, deltimes, deltimes));

	// Now let's hear it:
	output

}).play
)

// To stop it:
Ndef(\verb).free;