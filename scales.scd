//      //ukiyo-neko systems
// //   //
//          //  scales
// scales seeks to implement a major scale on one or several rows, while reserving the topmost row
// for changing the root of the scale, essentialy allowing us to paint all of the modes
//  // //    //
// // // // // //



// the difference between chords and chords2 is that chords2 has more sofisticated envelopes
// meaning that we want an envelope that lets us keep the key pressed
// which means that we have to mess with gates


// synth plugin
// to select a synth just select it and evaluate it
// they're all named wave1 so you dont have to change the keyboard code

// simple sine synth
(
SynthDef("wave1", {arg freq=440, release=0.1;
  var x,y;
  x =  SinOsc.ar(freq,freq,0.4)*EnvGen.kr(Env.adsr(0.1,release, 1))*EnvGate.new(0, doneAction:0);
  y = FreeVerb.ar(x,MouseX.kr(0,1),MouseY.kr(0,1), 0.1);
  Out.ar(0,y);
  Out.ar(1,y);
}).load(s);
)

s.scope;
// keyboard
// first generate the frequencies
(

   var a, root, dd,ee,ff, c, e,f ,seventh, third, fifth, freqs1, freqs2, freqs3, freqs4, piano, freqmod;


   // this part of the code is what constructs our keyboard
   // this is our basic frequency, we can shift it around if we so please
   var basefreq=438;

   // cuantas teclas asignamos
   var cuantos = 48;
   var f1 = 11; // number of keys in each row, going from lowest (z to highest, 1)
   var f2 = 12;
   var f3 = 12;
   var f4= 12;
   // freqs es donde se guarda la secuencia de frecuencias que generamos mas adelante
   freqs1=Array.fill([3,f1], { 1.0.rand.round(0.01) });

   freqs2=Array.newClear(f2); // frequency space
   freqs3=Array.newClear(f3); // frequency space
   freqs4=Array.newClear(f4); // frequency space


   piano=Array.newClear(200);// the unicode of each key stores the frequency that corresponds to a given mapping
   // it is necessary because the code that corresponds to each key does not follow any apparent mathematical
   // pattern, so the key assignments must be explicitly stated in a specific order, you will see it below

   a = Pseries(-5,1,inf); // generates values freom -24 to infinify
   dd = Pseries(0,1,inf); // generates values freom -24 to infinify
   ee = Pseries(3,1,inf); // generates values freom -24 to infinify

   ff = Pseries(5,1,inf); // generates values freom -24 to infinify

   /// note space
   // is defined by the following funcion

c = Scale.major.as(List) ;	// frequency generator function

third = basefreq * ((2**(1/12))**dd)  ;
   d = third.asStream;
// fifth = basefreq * ((2**(1/12))**ee) ;
fifth = basefreq * ((2**(1/12))**ee) ;

   f = fifth.asStream;
// seventh
seventh = basefreq * ((2**(1/12))**ff) ;
   e = seventh.asStream;

// fifth = basefreq * ((2**(1/12))**ee) ;
   f = fifth.asStream;

   c.next;
//   for (0, cuantos-1, { arg i; freqs1.put(i, c.next);});


///// this is the row assignment of frequencies
/////
/////
/////

// so.. so far we get the scale, but we want to map it to a freq, so we can put it in our synth freqs
// how do we do that?
// root
[60,122,120,99,118,98,110,109,44,46,47].do({|item, i| var n=c[i]; piano.put(item, Synth("wave1", [\freq, n])); piano.at(item).set(\gate,0);});
// third
[97,115,100,102,103,104,106,107,108,59,39,92].do({ arg item; piano.put(item, Synth("wave1", [\freq, d.next])); piano.at(item).set(\gate,0);});
// fifth
[113,119,101,114,116,121,117,105,111,112,91,93].do({ arg item; piano.put(item, Synth("wave1", [\freq, f.next])); piano.at(item).set(\gate,0);});
// seventh
[49,50,51,52,53,54,55,56,57,48,45,61].do({ arg item; piano.put(item, Synth("wave1", [\freq, e.next])); piano.at(item).set(\gate,0);});
for (0, piano.size, { arg i; piano.at(i).postln;});

//for (0, cuantos-1, { arg i; freqs.put(i, c.next); freqs.at(i).postln;});

// now assign them to the key values... mmm how to do this?

    z=Array.newClear(128); //create an array full of zeros
    w=Window.new;          // create window  and interface

    c=Slider(w, Rect(0,0,100,30)); // create a slider in said window

// how do we get to modulate a frequency with this setup?
   // freqmodslider= EZSlider(w, 300@50, "carrfreq", ControlSpec(0, 2, 'linear', 0.1, 0), {|ez|
//freqmod=ez.value});

//for (0, cuantos-1, { arg i; z.put(piano.at(unicode), c.next);});

c.keyDownAction =
{arg view, char, modifiers, unicode;
  piano.at(unicode).get(\freq,{arg value;("freq:" + value + "Hz").postln;});
  piano.at(unicode).set(\gate,1);
  };
c.keyUpAction =
{arg view, char, modifiers, unicode;
  piano.at(unicode).set(\gate,0);
};


    w.front;
w.onClose={piano.free;};	//action which stops running synth when the window close button is pressed

)
)


s.scope;




