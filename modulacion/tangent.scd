//      //ukiyo-neko systems
// //   //
//          //  keys sine keyboard v4 (tangent)
//  // //    //
// // // // // //



// tangent seeks to add a shift to the synth
// to accomplish this we have to first design the synth

// synth plugin
// to select a synth just highlight it and evaluate it
// they're all named wave1 so you dont have to change the keyboard code

// tangent sine synth
(
SynthDef("wave1", {arg freq=440, release=0.1;
  var x,y;
  x =  SinOsc.ar(Line.ar(start: 0.0, end: 1.0),freq,0.4)*EnvGen.kr(Env.adsr(0.1,release, 0.3))*EnvGate.new(0, doneAction:0);
  y = FreeVerb.ar(x,MouseX.kr(0,1),MouseY.kr(0,1), 0.1);
  Out.ar(0,y);
  Out.ar(1,y);
}).load(s);
)
// pan's voice  synth
(
SynthDef("wave1", {arg freq=400;
  var x;
  // calculate how to make a fifth from a note
  x=Pan2.ar(Mix(SinOsc.ar([freq,freq*1.5],0,0.1)),SinOsc.ar(2))*EnvGen.kr(Env.perc(0.5,1),doneAction:2);
  Out.ar(0,x);
  Out.ar(1,x);

}).load(s);
)
// the tuning is customizable so thats nice eh?





s.scope;
// keyboard
// first generate the frequencies
(

   var a, b, c, d, freqs, piano, aadfflist, freqmod, freqmodslider;


   // this part of the code is what constructs our keyboard
   // this is our basic frequency, we can shift it around if we so please
   var basefreq=438;

   // cuantas teclas asignamos
   var cuantos = 48;
   // freqs es donde se guarda la secuencia de frecuencias que generamos mas adelante
   freqs=Array.newClear(cuantos); // frequency space

   piano=Array.newClear(200);// the unicode of each key stores the frequency that corresponds to a given mapping
   // it is necessary because the code that corresponds to each key does not follow any apparent mathematical
   // pattern, so the key assignments must be explicitly stated in a specific order, you will see it below

   a = Pseries(-24,1,inf); // generates values freom -24 to infinify

   /// note space
   // is defined by the following funcion

   b = basefreq * ((2**(1/12))**a) ;	// frequency generator function
   c = b.asStream;
   d = b.asStream; // d se usa para generar otro arreglo

   c = b.asStream;
   c.next;
   for (0, cuantos-1, { arg i; freqs.put(i, c.next);});

[122,120,99,118,98,110,109,44,46,47,97,115,100,102,103,104,106,107,108,59,39,92,113,119,101,114,116,121,117,105,111,112,91,93,49,50,51,52,53,54,55,56,57,48,45,61].do({ arg item; piano.put(item, Synth("wave1", [\freq, d.next])); piano.at(item).set(\gate,0);});
for (0, piano.size, { arg i; piano.at(i).postln;});

//for (0, cuantos-1, { arg i; freqs.put(i, c.next); freqs.at(i).postln;});

// now assign them to the key values... mmm how to do this?

    z=Array.newClear(128); //create an array full of zeros
    w=Window.new;          // create window  and interface

    c=Slider(w, Rect(0,0,100,30)); // create a slider in said window

// how do we get to modulate a frequency with this setup?
   // freqmodslider= EZSlider(w, 300@50, "carrfreq", ControlSpec(0, 2, 'linear', 0.1, 0), {|ez|
//freqmod=ez.value});

//for (0, cuantos-1, { arg i; z.put(piano.at(unicode), c.next);});

c.keyDownAction =
{arg view, char, modifiers, unicode;
  piano.at(unicode).postln;
  piano.at(unicode).set(\gate,1);
  };
c.keyUpAction =
{arg view, char, modifiers, unicode;
  piano.at(unicode).set(\gate,0);
};


    w.front;
)
)


s.scope;




