
// Modulo
// ukiyo-neko systems
//
// un modulador simple, tiene tres perillas, la frecuencia base, la frecuencia de modulacion y la apmlitud de modulacion.. y dos perillas para la modulacion de modfreq
(
SynthDef(\modulo,{arg carrfreq=440, modfreq=1, moddepth=0.01, modfreqfreq=1, modfreqdepth=0, moddepthfreq=0, moddepthdepth=0;   //make sure there are control arguments to affect!
	Out.ar(0,
    SinOsc.ar(carrfreq + (moddepth*SinOsc.ar(modfreq+(modfreqdepth*SinOsc.ar( SinOsc.ar(moddepthfreq)*moddepthdepth+modfreqfreq)))),0,0.25)!2)
}).add;
)

s.scope;

(

var w, carrfreqslider, modfreqslider, moddepthslider, modfreqfreqslider, modfreqfreqslider2, modfreqdepthslider,moddepthfreqslid, moddepthdepthslid, synth;

w=Window("frequency modulation", Rect(100, 400, 400, 300));
w.view.decorator = FlowLayout(w.view.bounds);

a = Synth(\modulo);

carrfreqslider= EZSlider(w, 300@50, "carrfreq", ControlSpec(5, 5000, 'linear', 10, 440), {|ez|
a.set(\carrfreq, ez.value)});
w.view.decorator.nextLine;

modfreqslider= EZSlider(w, 300@50, "modfreq", ControlSpec(1, 2000, 'linear', 1, 1), {|ez|  a.set(\modfreq, ez.value)});
w.view.decorator.nextLine;


modfreqfreqslider= EZSlider(w, 300@50, "modfreqfreq", ControlSpec(20, 5000, 'linear', 10, 440), {|ez|
a.set(\modfreqfreq, ez.value)});
w.view.decorator.nextLine;


modfreqfreqslider2= EZSlider(w, 300@50, "modfreqfreq", ControlSpec(0, 100, 'linear', 1, 440), {|ez|
a.set(\modfreqfreq, ez.value)});
w.view.decorator.nextLine;

modfreqdepthslider= EZSlider(w, 300@50, "modfreqdepth", ControlSpec(20, 10000, 'linear', 10, 440), {|ez|
a.set(\modfreqdepth, ez.value)});
w.view.decorator.nextLine;

moddepthslider= EZSlider(w, 300@50, "moddepth", ControlSpec(0.01, 5000, 'linear', 0.01, 0.01), {|ez|  a.set(\moddepth, ez.value)});

moddepthfreqslid= EZSlider(w, 300@50, "moddepthfreqslid", ControlSpec(0.01, 500, 'linear', 0.01, 0.01), {|ez|  a.set(\moddepthfreq, ez.value)});
w.view.decorator.nextLine;

moddepthdepthslid= EZSlider(w, 300@50, "moddepthdepthslid", ControlSpec(0.01, 200, 'linear', 0.01, 0.01), {|ez|  a.set(\moddepthdepth, ez.value)});
w.view.decorator.nextLine;


w.front;
)
