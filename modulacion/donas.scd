
// donas
// ukiyo-neko systems
//
// un modulador simple, tiene tres perillas, la frecuencia base, la frecuencia de modulacion y la apmlitud de modulacion.. y tiene un delay con feedback
(
SynthDef(\modulo,{arg carrfreq=440, modfreq=1, moddepth=0.01, delaytime=0, decaytime=1, mul=1;
  var mix;
  mix = SinOsc.ar(carrfreq + (moddepth*SinOsc.ar(modfreq)),0,0.25);
  Out.ar(0, CombN.ar(mix,2, delaytime, decaytime, mul, mix)!2);


}).add;
)

s.scope;

(

var w, carrfreqslider, modfreqslider, moddepthslider, delayS, decayS, mulS, synth;

w=Window("frequency modulation", Rect(100, 400, 400, 300));
w.view.decorator = FlowLayout(w.view.bounds);

a = Synth(\modulo);

carrfreqslider= EZSlider(w, 300@50, "carrfreq", ControlSpec(20, 5000, 'linear', 10, 440), {|ez|
a.set(\carrfreq, ez.value)});
w.view.decorator.nextLine;

modfreqslider= EZSlider(w, 300@50, "modfreq", ControlSpec(1, 2000, 'linear', 1, 1), {|ez|  a.set(\modfreq, ez.value)});

w.view.decorator.nextLine;
moddepthslider= EZSlider(w, 300@50, "moddepth", ControlSpec(0.01, 5000, 'linear', 0.01, 0.01), {|ez|  a.set(\moddepth, ez.value)});

w.view.decorator.nextLine;
delayS= EZSlider(w, 300@50, "delay", ControlSpec(0, 2, 'linear', 0.1, 1), {|ez|  a.set(\delaytime, ez.value)});

w.view.decorator.nextLine;
decayS= EZSlider(w, 300@50, "decay", ControlSpec(0, 2, 'linear',0.1, 1), {|ez|  a.set(\decaytime, ez.value)});
w.view.decorator.nextLine;
mulS= EZSlider(w, 300@50, "mul", ControlSpec(0, 1.1, 'linear',0.1, 1), {|ez|  a.set(\mul, ez.value)});




w.front;
)

