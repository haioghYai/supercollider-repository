
// Modulo keys v2 stable
// ukiyo-neko systems
//
// un modulador con un teclado, las frecuencias de modulacion estan definidas en una funcion mas adelante
// works as expected
(
SynthDef(\modulo,{arg carrfreq=440, modfreq=10, moddepth=0.8, release=0.1;   //make sure there are control arguments to affect!
	var x,y;
  x = SinOsc.ar(carrfreq * (moddepth*SinOsc.ar(modfreq)),0,0.25);
  y = x *EnvGen.kr(Env.adsr(0.1,release, 0.3))*EnvGate.new(0, doneAction:0);
  Out.ar(0,
    [y,y]);
}).add;
)

s.scope;

(

   var a, b, c, d, e, f,freqs, mfreqs, piano, modfreq, freqdepth;


   // this part of the code is what constructs our keyboard
   // this is our basic frequency, we can shift it around if we so please
   var basefreq=438;
   var basefrqmod=1;
   var basefreqdepth=1;
//var baseModFreq=440;
var baseModFreq=440;

   // cuantas teclas asignamos
   var cuantos = 48;
   // freqs es donde se guarda la secuencia de frecuencias que generamos mas adelante
   freqs=Array.newClear(cuantos); // frequency space

   piano=Array.newClear(200);// the unicode of each key stores the frequency that corresponds to a given mapping
   // it is necessary because the code that corresponds to each key does not follow any apparent mathematical
   // pattern, so the key assignments must be explicitly stated in a specific order, you will see it below

   a = Pseries(-24,1,inf); // generates values freom -24 to infinify

   /// note space
// is defined by the following funcion

   b = basefreq * ((2**(1/12))**a) ;	// frequency generator function
   c = b.asStream;
   d = b.asStream; // d se usa para generar otro arreglo
//   f = Pseries(-24,1,inf); // generates values freom -24 to infinify

     e = baseModFreq*((2**(1/12))**Pseries(-24,1, inf) ).asStream;
   c.next;
   for (0, cuantos-1, { arg i; freqs.put(i, c.next);}); // guarda los numeros generados en el arreglo


[122,120,99,118,98,110,109,44,46,47,97,115,100,102,103,104,106,107,108,59,39,92,113,119,101,114,116,121,117,105,111,112,91,93,49,50,51,52,53,54,55,56,57,48,45,61].do({ arg item; piano.put(item, Synth("modulo", [\carrfreq, d.next])); piano.at(item).set(\gate,0);});
"a".postln;
[122,120,99,118,98,110,109,44,46,47,97,115,100,102,103,104,106,107,108,59,39,92,113,119,101,114,116,121,117,105,111,112,91,93,49,50,51,52,53,54,55,56,57,48,45,61].do({arg item; piano.at(item).set(\modfreq, e.next); piano.at(item).get(\modfreq, {arg value; ("modfreq is" + value + "Hz").postln});
piano.at(item).set(\gate,0);});

for (0, piano.size, { arg i; piano.at(i).get(\modfreq,{arg value;("freq is not:" + value + "Hz").postln;})});
x.get(\freq, { arg value; ("freq is now:" + value + "Hz").postln; });
//for (0, cuantos-1, { arg i; freqs.put(i, c.next); freqs.at(i).postln;});

// now assign them to the key values... mmm how to do this?

    z=Array.newClear(128); //create an array full of zeros
    w=Window.new;          // create window  and interface

    c=Slider(w, Rect(0,0,100,30)); // create a slider in said window

// how do we get to modulate a frequency with this setup?
   // freqmodslider= EZSlider(w, 300@50, "carrfreq", ControlSpec(0, 2, 'linear', 0.1, 0), {|ez|
//freqmod=ez.value});

//for (0, cuantos-1, { arg i; z.put(piano.at(unicode), c.next);});

c.keyDownAction =
{arg view, char, modifiers, unicode;
  piano.at(unicode).get(\modfreq,{arg value;("freq:" + value + "Hz").postln;});
  piano.at(unicode).set(\gate,1);
  //piano.at(unicode).set;
  };
c.keyUpAction =
{arg view, char, modifiers, unicode;
  piano.at(unicode).set(\gate,0);
};


    w.front;
)
)



(

var w, carrfreqslider, modfreqslider, moddepthslider, synth;

w=Window("frequency modulation", Rect(100, 400, 400, 300));
w.view.decorator = FlowLayout(w.view.bounds);

a = Synth(\modulo);

carrfreqslider= EZSlider(w, 300@50, "carrfreq", ControlSpec(20, 5000, 'linear', 10, 440), {|ez|
a.set(\carrfreq, ez.value)});
w.view.decorator.nextLine;

modfreqslider= EZSlider(w, 300@50, "modfreq", ControlSpec(1, 2000, 'linear', 1, 1), {|ez|  a.set(\modfreq, ez.value)});

w.view.decorator.nextLine;
moddepthslider= EZSlider(w, 300@50, "moddepth", ControlSpec(0.01, 5000, 'linear', 0.01, 0.01), {|ez|  a.set(\moddepth, ez.value)});

w.front;
)

(

var w, carrfreqslider, modfreqslider, moddepthslider, synth;

w=Window("frequency modulation", Rect(100, 400, 400, 300));
w.view.decorator = FlowLayout(w.view.bounds);

b = Synth(\modulo);

carrfreqslider= EZSlider(w, 300@50, "carrfreq", ControlSpec(20, 5000, 'linear', 10, 440), {|ez|
b.set(\carrfreq, ez.value)});
w.view.decorator.nextLine;

modfreqslider= EZSlider(w, 300@50, "modfreq", ControlSpec(1, 2000, 'linear', 1, 1), {|ez|  b.set(\modfreq, ez.value)});

w.view.decorator.nextLine;
moddepthslider= EZSlider(w, 300@50, "moddepth", ControlSpec(0.01, 5000, 'linear', 0.01, 0.01), {|ez|  b.set(\moddepth, ez.value)});

w.front;
)


(

var w, carrfreqslider, modfreqslider, moddepthslider, synth;

w=Window("frequency modulation", Rect(100, 400, 400, 300));
w.view.decorator = FlowLayout(w.view.bounds);

c= Synth(\modulo);

carrfreqslider= EZSlider(w, 300@50, "carrfreq", ControlSpec(20, 5000, 'linear', 10, 440), {|ez|
c.set(\carrfreq, ez.value)});
w.view.decorator.nextLine;

modfreqslider= EZSlider(w, 300@50, "modfreq", ControlSpec(1, 2000, 'linear', 1, 1), {|ez|  c.set(\modfreq, ez.value)});

w.view.decorator.nextLine;
moddepthslider= EZSlider(w, 300@50, "moddepth", ControlSpec(0.01, 5000, 'linear', 0.01, 0.01), {|ez|  c.set(\moddepth, ez.value)});

w.front;
)