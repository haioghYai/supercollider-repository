
// Modkey
// ukiyo-neko systems
//
// modkey es una integracion de un teclado a modulo
(




SynthDef(\modulo,{arg carrfreq=440, modfreq=10, moddepth=0.5, release=0.1;

	Out.ar(0,
    //
  SinOsc.ar(carrfreq + SinOsc.ar(modfreq,0,moddepth),0,0.25)!2)*EnvGen.kr(Env.adsr(0.1,release, 0.3))*EnvGate.new(0, doneAction:0)


}).add;
)






s.scope;

(

var w, carrfreqslider, m1control, m1addslider, synth;

//////////////////////
//////////////////////
// keyboard construction
////////////////////////
////////////////////////

   var basefreq=220;
   var cuantos = 48; // cuantas teclas
   var f1 = 10; // primera fila
   var f2 = 12; // etc
   var f3 = 12;
    var f4= 12;

// freqs es donde se guarda la secuencia de frecuencias que generamos mas adelante
   freqs1=Array.newClear(f1); // frequency space
   freqs2=Array.newClear(f2); // frequency space
   freqs3=Array.newClear(f3); // frequency space
   freqs4=Array.newClear(f4); // frequency space

   // the following code would be much better with a dictionary
   piano=Array.newClear(200);// the unicode of each key stores the frequency that corresponds to a given mapping
   // it is necessary because the code that corresponds to each key does not follow any apparent mathematical
   // pattern, so the key assignments must be explicitly stated in a specific order, you will see it below

   var a, root, dd,ee,ff, c, e,f ,seventh, third, fifth, freqs1, freqs2, freqs3, freqs4, piano, freqmod;


   // this part of the code is what constructs our keyboard
   // this is our basic frequency, we can shift it around if we so please




   piano=Array.newClear(200);// the unicode of each key stores the frequency that corresponds to a given mapping
   // it is necessary because the code that corresponds to each key does not follow any apparent mathematical
   // pattern, so the key assignments must be explicitly stated in a specific order, you will see it below

   a = Pseries(-5,1,inf); // generates values freom -24 to infinify
   dd = Pseries(0,1,inf); // generates values freom -24 to infinify
   ee = Pseries(3,1,inf); // generates values freom -24 to infinify

   ff = Pseries(5,1,inf); // generates values freom -24 to infinify

   /// note space
   // is defined by the following funcion

   root = basefreq * ((2**(1/12))**a) ;	// frequency generator function
   c = root.asStream;
third = basefreq * ((2**(1/12))**dd)  ;
   d = third.asStream;
// fifth = basefreq * ((2**(1/12))**ee) ;
fifth = basefreq * ((2**(1/12))**ee) ;

   f = fifth.asStream;
// seventh
seventh = basefreq * ((2**(1/12))**ff) ;
   e = seventh.asStream;

// fifth = basefreq * ((2**(1/12))**ee) ;
   f = fifth.asStream;

   c.next;
//   for (0, cuantos-1, { arg i; freqs1.put(i, c.next);});


///// this is the row assignment of frequencies
/////
/////
/////


// root
[122,120,99,118,98,110,109,44,46,47].do({ arg item; var n=c.next; piano.put(item, Synth("wave1", [\freq, n])); piano.at(item).set(\gate,0);});
// third
[97,115,100,102,103,104,106,107,108,59,39,92].do({ arg item; piano.put(item, Synth("wave1", [\freq, d.next])); piano.at(item).set(\gate,0);});
// fifth
[113,119,101,114,116,121,117,105,111,112,91,93].do({ arg item; piano.put(item, Synth("wave1", [\freq, f.next])); piano.at(item).set(\gate,0);});
// seventh
[49,50,51,52,53,54,55,56,57,48,45,61].do({ arg item; piano.put(item, Synth("wave1", [\freq, e.next])); piano.at(item).set(\gate,0);});
for (0, piano.size, { arg i; piano.at(i).postln;});

//for (0, cuantos-1, { arg i; freqs.put(i, c.next); freqs.at(i).postln;});

// now assign them to the key values... mmm how to do this?

    z=Array.newClear(128); //create an array full of zeros
    w=Window.new;          // create window  and interface

    c=Slider(w, Rect(0,0,100,30)); // create a slider in said window

// how do we get to modulate a frequency with this setup?
   // freqmodslider= EZSlider(w, 300@50, "carrfreq", ControlSpec(0, 2, 'linear', 0.1, 0), {|ez|
//freqmod=ez.value});

//for (0, cuantos-1, { arg i; z.put(piano.at(unicode), c.next);});

c.keyDownAction =
{arg view, char, modifiers, unicode;
  piano.at(unicode).get(\freq,{arg value;("freq:" + value + "Hz").postln;});
  piano.at(unicode).set(\gate,1);
  };
c.keyUpAction =
{arg view, char, modifiers, unicode;
  piano.at(unicode).set(\gate,0);
};


    w.front;
w.onClose={piano.free;};	//action which stops running synth when the window close button is pressed

)
)
//////////////////////////////////

	//////////////////////
	// Initialize Windows
	//////////////////////

	Window.closeAll;
s.scope;

// we have the possibility of creating subwindows
/*	subwin = FlowView.new(
		parent: win,
		bounds: Rect(710, 230, 185, 150),
		margin: 10@10,
		gap: 10@10;
	);*/


  ////////////////////
  // window creation
  ////////////////////
w=Window("frequency modulation", Rect(100, 500, 400, 300),resizable: false);
w.view.decorator = FlowLayout(w.view.bounds);
w.front;
w.onClose = {s.freeAll};

	// What to do on close (or ctrl+period)
	CmdPeriod.doOnce({Window.closeAll});


//// window aesthetics
w.background = Color.new(1,1,1,1);
w.alpha = 0.95;


///////////////
// synth creation/
//////////////////
a = Synth(\modulo);




// create the slider with EXSlider carrfreqslider is how we address it
carrfreqslider= EZSlider(w, 300@50, "carrfreq", ControlSpec(20, 5000, 'linear', 10, 440), {|ez|
a.set(\carrfreq, ez.value)});
w.view.decorator.nextLine;

w.view.decorator.nextLine;

m1control =  Slider2D(w,Rect(10,10,180,180));
// modfreqslider= EZSlider(w, 300@50, "modfreq", ControlSpec(1, 2000, 'linear', 1, 1), {|ez|  a.set(\modfreq, ez.value)});
//
// w.view.decorator.nextLine;
// moddepthslider= EZSlider(w, 300@50, "moddepth", ControlSpec(0.01, 5000, 'linear', 0.01, 0.01), {|ez|  a.set(\moddepth, ez.value)});

m1control.action = {

  a.set(\modfreq,m1control.x*100.0+1);//\moddepth,m1control.y*0.5+0.01).postln;
  a.set(\moddepth,m1control.y*1000.0);//\moddepth,m1control.y*0.5+0.01).postln;

  a.get(\modfreq,{arg value;("freq:" + value + "Hz").postln;});
  a.get(\moddepth,{arg value;("depth:" + value + "Hz").postln;});

};


w.front;
)



(

var w, carrfreqslider, modfreqslider, moddepthslider, synth;

w=Window("frequency modulation", Rect(100, 400, 400, 300));
w.view.decorator = FlowLayout(w.view.bounds);

c= Synth(\modulo);

carrfreqslider= EZSlider(w, 300@50, "carrfreq", ControlSpec(20, 5000, 'linear', 10, 440), {|ez|
c.set(\carrfreq, ez.value)});
w.view.decorator.nextLine;

modfreqslider= EZSlider(w, 300@50, "modfreq", ControlSpec(1, 2000, 'linear', 1, 1), {|ez|  c.set(\modfreq, ez.value)});

w.view.decorator.nextLine;
moddepthslider= EZSlider(w, 300@50, "moddepth", ControlSpec(0.01, 5000, 'linear', 0.01, 0.01), {|ez|  c.set(\moddepth, ez.value)});

w.front;
)