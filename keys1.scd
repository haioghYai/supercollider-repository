//      //ukiyo-neko systems
// //   //
//          //  keys sine keyboard v1
//  // //    //
// // // // // //

(
SynthDef("wave1", {arg freq=440;
  var x;
  x = SinOsc.ar(freq,freq,0.4);
  Out.ar(0,x);
  Out.ar(1,x);
}).load(s);
)


// this is akin to a keyboard, but the tuning is completely out of wack
// how do we get a 'nice' tuning?

s.scope;
// keyboard
// first generate the frequencies
(

   var a, b, c, d, freqs, piano, list, freqmodslider, freq;
   var basefreq=440;
   var cuantos = 48;
   freqs=Array.newClear(cuantos); // frequency space
   piano=Array.newClear(200);// this array stores the frequency that corresponds to all keys
// each uni

   a = Pseries(-24,1,inf); // stepper function
   b = basefreq * ((2**(1/12))**a) ;	// pattern b es el cuadrado del pattern a
   c = b.asStream;
   d = b.asStream; // d se usa para generar otro arreglo

   c = b.asStream;
   c.next;
   for (0, cuantos-1, { arg i; freqs.put(i, c.next);});

[122,120,99,118,98,110,109,44,46,47,97,115,100,102,103,104,106,107,108,59,39,92,113,119,101,114,116,121,117,105,111,112,91,93,49,50,51,52,53,54,55,56,57,48,45,61].do({ arg item, i; piano.put(item, d.next).postln});
for (0, piano.size, { arg i; piano.at(i).postln;});

//for (0, cuantos-1, { arg i; freqs.put(i, c.next); freqs.at(i).postln;});

// now assign them to the key values... mmm how to do this?

    z=Array.newClear(128); //create an array full of zeros
    w=Window.new;          // create window  and interface
    c=Slider(w, Rect(0,0,100,30)); // create a slider in said window

    c.keyDownAction = {arg view, char, modifiers, unicode, keycode;
      // the slider detects key actions and acts on that
      // if z is empty
      if(z.at(unicode).isNil,
        {
          // post the unicode
          //unicode.postln;
          piano.at(unicode).postln;
          // now we put in the array a synth object..
          // that means that by putting a synth in an array
          // its the same as playing it..

          z.put(unicode,
            Synth("wave1",
            [\freq, piano.at(unicode)] // we need to make a code to translate unicode -> frequencies
              // for this I need to determine my frequency array...
            )
          );
        },
        {
          z.at(unicode).free;
          z.put(unicode, nil);
        }
      );
    };
    w.front;
)


s.scope;
['a', 'b', 'c'].do({ arg item, i; [i, item].postln; });
















// this creates an envelope viewer..?
// its pretty werd
(
// use shift-click to keep a node selected
w = Window("envelope", Rect(150 , Window.screenBounds.height - 250, 250, 100)).front;
w.view.decorator = FlowLayout(w.view.bounds);

b = EnvelopeView(w, Rect(0, 0, 230, 80))
    .drawLines_(true)
    .selectionColor_(Color.red)
    .drawRects_(true)
    .resize_(5)
    .step_(0.05)
    .action_({arg b; [b.index, b.value].postln})
    .thumbSize_(5)
    .value_([[0.0, 0.1, 0.5, 1.0],[0.1,1.0,0.8,0.0]]);
w.front;
)
(
a = Window("text-boxes", Rect(200 , 450, 450, 450));
a.view.decorator = FlowLayout(a.view.bounds);

b = EnvelopeView(a, Rect(0, 0, 440, 440))
    .thumbWidth_(60.0)
    .thumbHeight_(15.0)
    .drawLines_(true)
    .drawRects_(true)
    .selectionColor_(Color.red)
    .value_([[0.1, 0.4, 0.5, 0.3], [0.1, 0.2, 0.9, 0.7]]);
4.do({arg i;
    b.setString(i, ["this", "is", "so much", "fun"].at(i));
    b.setFillColor(i,[Color.yellow, Color.white, Color.green].choose);
});
a.front;
)

(
b.connect(3, [2.0,0.0,1.0]); // the text objects can be connected
b.connect(0,[2.0,3.0,1.0]);
)