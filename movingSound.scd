//////////
///////////////
////////////////

// sinte cero

(
SynthDef(\cero,{arg freq1=200, freq2=200, phase1=0, phase2=0;   //make sure there are control arguments to affect!
 // var x, y;
  // x = SinOsc.ar(freq1, phase1);
  // y = SinOsc.ar(freq2, phase2) ;
  Out.ar(0, [SinOsc.ar(freq1, phase1),SinOsc.ar(freq2, phase2)])


}).add;

)

// ejecutor cero
s.scope;

(

var w, f1S, f2S, p1S, p2S;


a = Synth("cero");


///// GUI//////////////
///////////////////
////////////////////////////
w=Window("sineVisualizer", Rect(100, 400, 400, 300));

w.view.decorator = FlowLayout(w.view.bounds);

f1S= EZSlider(w, 300@50, "freq 1", ControlSpec(0, 2000, 'linear',0.1, 200), {|ez|
a.set(\freq1, ez.value)});
w.view.decorator.nextLine;

p1S= EZSlider(w, 300@50, "phase 1", ControlSpec(0, 2*pi, 'linear',0.1, 0), {|ez|
a.set(\phase1, ez.value)});
w.view.decorator.nextLine;

f2S= EZSlider(w, 300@50, "freq2", ControlSpec(0, 2000, 'linear', 0.10, 200), {|ez|
a.set(\freq2, ez.value)});
w.view.decorator.nextLine;


p2S= EZSlider(w, 300@50, "phase 2", ControlSpec(0, 2*pi, 'linear',0.1, 0), {|ez|
a.set(\phase2, ez.value)});

w.front;


)









//////////
///////////////
////////////////

// sinte uno

(
SynthDef(\circulo,{arg moddepth=1, modfreq=200;   //make sure there are control arguments to affect!
  var x, y;
      x = SinOsc.ar(MouseX.kr(0,600) + (moddepth*SinOsc.ar(modfreq)));
      y = SinOsc.ar(MouseY.kr(0,600) + (moddepth*SinOsc.ar(modfreq))) ;
  Out.ar(0, [x,y])


}).add;

)

// ejecutor uno
s.scope;

(

var w, carrfreqslider, mDS, mFS, synth;



a = Synth("circulo");


///// GUI//////////////
///////////////////
////////////////////////////
w=Window("frequency modulation", Rect(100, 400, 400, 300));

w.view.decorator = FlowLayout(w.view.bounds);

mDS= EZSlider(w, 300@50, "modDepth", ControlSpec(0, 200, 'linear',0.1, 1), {|ez|
a.set(\moddepth, ez.value)});
w.view.decorator.nextLine;

mFS= EZSlider(w, 300@50, "modFreq", ControlSpec(0, 1000, 'linear', 0.10, 1), {|ez|
a.set(\modfreq, ez.value)});
w.front;


)



//////
///
////// sinte dos
//
/////


// sinte dos
(
SynthDef(\circulo,{arg moddepth=1, modfreq=200, depthFreq=1;   //make sure there are control arguments to affect!
  var x, y;
      x = SinOsc.ar(depthFreq)*moddepth*SinOsc.ar(modfreq) * SinOsc.ar(MouseX.kr(0,600),0,0.25);
      y = SinOsc.ar(depthFreq)*moddepth*SinOsc.ar(modfreq) * SinOsc.ar(MouseY.kr(0,600),0,0.25);
  Out.ar(0, [x,y])


}).add;

)
///
///
// exe 2
////

(

var w, carrfreqslider, mDS, mFS, synth;



a = Synth("circulo");


///// GUI//////////////
///////////////////
////////////////////////////
w=Window("frequency modulation", Rect(100, 400, 400, 300));

w.view.decorator = FlowLayout(w.view.bounds);

mDS= EZSlider(w, 300@50, "modDepth", ControlSpec(0, 10, 'linear',0.1, 1), {|ez|
a.set(\moddepth, ez.value)});
w.view.decorator.nextLine;

mFS= EZSlider(w, 300@50, "modFreq", ControlSpec(0, 3000, 'linear', 0.10, 1), {|ez|
a.set(\modfreq, ez.value)});
w.front;


)





(
{
    // rotation via mouse
    var x, y;
      x = SinOsc.ar(MouseX.kr(0,600));
      y = SinOsc.ar(MouseY.kr(0,600)) ;

    #x, y = Rotate2.ar(x, y);
    [x,y]
}.play;
)


(
{
    // rotation via mouse
    var x, y;
      x = LFTri.ar(MouseX.kr(0,600));
      y = LFTri.ar(MouseY.kr(0,600)) ;

    #x, y = Rotate2.ar(x, y);
    [x,y]
}.play;
)

(
{
    // rotation via mouse
    var x, y;
  x = LFPulse.ar(MouseX.kr(0,600));
      y = LFPulse.ar(MouseY.kr(0,600)) ;

    #x, y = Rotate2.ar(x, y);
    [x,y]
}.play;
)

// otras cosas
(
// rotación vía un oscilador a baja frecuencia:
 {	var senial, y;

  Rotate2.ar(SinOsc.ar(MouseY.ar(0,5000)), SinOsc.ar(MouseX.ar(0,5000)));
 }.play;
)

(
{
    // rotation via mouse
    var x, y;
    x = Mix.fill(4, { LFSaw.ar(200 + 2.0.rand2, 0, 0.1) });
    y = SinOsc.ar(900) * LFPulse.kr(3,0,0.3,0.2);
    #x, y = Rotate2.ar(x, y, MouseX.kr(0,2));
    [x,y]
}.scope;
)
